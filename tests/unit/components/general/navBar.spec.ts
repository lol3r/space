import { expect } from "chai";
import { mount } from "@vue/test-utils";
import navBar from "@/components/general/navBar.vue";

describe("navBar.vue", () => {
  it("Displays both views", async () => {
    const wrapper = mount(navBar);

    const html = wrapper.html();

    expect(html).to.contains("Home");
    expect(html).to.contains("Favorites");
  });
});
