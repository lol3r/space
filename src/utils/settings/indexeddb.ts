function hasIndexedDB(): boolean {
  return typeof indexedDB !== "undefined";
}

export function saveToIndexedDB(favorites: Array<NASAImage>) {
  if (!hasIndexedDB()) {
    return;
  }

  const request = indexedDB.open("space", 1);
  request.onerror = event => {
    console.log(event);
  };
  request.onupgradeneeded = () => {
    request.result.createObjectStore("favorites");
  };
  request.onsuccess = () => {
    const db = request.result;

    const transaction = db.transaction("favorites", "readwrite");
    const objectStore = transaction.objectStore("favorites");

    for (const image of favorites) {
      objectStore.put(JSON.stringify(image), image.pageURL);
    }
  };
}

export function loadFavoritesFromIndexedDB(): Promise<Array<NASAImage>> {
  return new Promise<Array<NASAImage>>((resolve, reject) => {
    if (!hasIndexedDB()) {
      reject();
      return;
    }

    const request = indexedDB.open("space", 1);
    request.onerror = () => {
      reject();
    };
    request.onupgradeneeded = () => {
      request.result.createObjectStore("favorites");
    };
    request.onsuccess = () => {
      const db = request.result;

      const transaction = db.transaction("favorites", "readonly");
      const objectStore = transaction.objectStore("favorites");

      const loadReq = objectStore.getAll();
      loadReq.onerror = () => {
        reject();
      };
      loadReq.onsuccess = () => {
        const favorites = new Array<NASAImage>();

        for (const part of loadReq.result) {
          const raw = JSON.parse(part);
          favorites.push(raw);
        }

        resolve(favorites);
      };
    };
  });
}

export function deleteFromIndexedDB(removePageURL: string): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    if (!hasIndexedDB()) {
      reject();
      return;
    }

    const request = indexedDB.open("space", 1);
    request.onerror = () => {
      reject();
    };
    request.onupgradeneeded = () => {
      request.result.createObjectStore("favorites");
    };
    request.onsuccess = () => {
      const db = request.result;

      const transaction = db.transaction("favorites", "readwrite");
      const objectStore = transaction.objectStore("favorites");

      const deleteReq = objectStore.delete(removePageURL);
      deleteReq.onerror = () => {
        reject();
      };
      deleteReq.onsuccess = () => {
        resolve();
      };
    };
  });
}
