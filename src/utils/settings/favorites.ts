export function addToFavorites(
  settings: Settings,
  nImage: NASAImage
): Settings {
  for (const tmpFav of settings.Favorites) {
    if (tmpFav.pageURL == nImage.pageURL) {
      return settings;
    }
  }

  settings.Favorites.push(nImage);
  return settings;
}

export function removeFromFavorites(
  settings: Settings,
  pageURL: string
): Settings {
  let index = -1;
  for (const tmpIndex in settings.Favorites) {
    if (settings.Favorites[tmpIndex].pageURL == pageURL) {
      index = parseInt(tmpIndex, 10);
      break;
    }
  }
  if (index < 0) {
    return settings;
  }

  settings.Favorites.splice(index, 1);
  return settings;
}
