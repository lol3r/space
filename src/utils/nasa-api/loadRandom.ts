import Axios from "axios";

export function loadRandomImage(): Promise<NASAImage> {
  const url = `https://space.lol3r.net/api/image/random`;

  return new Promise<NASAImage>((resolve, reject) => {
    Axios.get(url)
      .then(resp => {
        const data = resp.data;

        resolve(data);
      })
      .catch(reject);
  });
}
