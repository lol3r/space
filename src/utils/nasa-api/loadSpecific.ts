import Axios from "axios";

export function loadSpecific(date: NasaDate): Promise<NASAImage> {
  const url = `https://space.lol3r.net/api/image/day?year=${date.year}&month=${date.month}&day=${date.day}`;

  return new Promise<NASAImage>((resolve, reject) => {
    Axios.get(url)
      .then(resp => {
        const data = resp.data;

        resolve(data);
      })
      .catch(reject);
  });
}
