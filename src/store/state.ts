import { State } from "vue";

export function createState(): State {
  return {
    currentImage: undefined,
    settings: {
      Favorites: new Array<NASAImage>(0)
    } as Settings
  };
}
