import { State } from "vue";
import { ActionContext, ActionTree } from "vuex";
import { ActionTypes } from "./action-types";
import { Mutations } from "./mutations";
import { MutationTypes } from "./mutation-types";
import { loadRandomImage } from "@/utils/nasa-api/loadRandom";
import {
  saveToIndexedDB,
  loadFavoritesFromIndexedDB
} from "@/utils/settings/indexeddb";
import { loadSpecific } from "@/utils/nasa-api/loadSpecific";

export type AugmentedActionContext = {
  commit<K extends keyof Mutations>(
    key: K,
    payload: Parameters<Mutations[K]>[1]
  ): ReturnType<Mutations[K]>;
} & Omit<ActionContext<State, State>, "commit">;

export interface Actions {
  [ActionTypes.LOAD_RANDOM](context: AugmentedActionContext): Promise<void>;
  [ActionTypes.LOAD_SPECIFIC](
    context: AugmentedActionContext,
    date: NasaDate
  ): Promise<void>;
  [ActionTypes.LOAD_FAVORITES](context: AugmentedActionContext): Promise<void>;
  [ActionTypes.SAVE_FAVORITES](context: AugmentedActionContext): Promise<void>;
}

export const actions: ActionTree<State, State> & Actions = {
  [ActionTypes.LOAD_RANDOM](context: AugmentedActionContext) {
    return new Promise<void>((resolve, reject) => {
      loadRandomImage()
        .then(nImage => {
          context.commit(MutationTypes.UPDATE_CURRENT_IMAGE, nImage);
          resolve();
        })
        .catch(reject);
    });
  },

  [ActionTypes.LOAD_SPECIFIC](
    context: AugmentedActionContext,
    date: NasaDate
  ): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      loadSpecific(date)
        .then(nImage => {
          context.commit(MutationTypes.UPDATE_CURRENT_IMAGE, nImage);
          resolve();
        })
        .catch(reject);
    });
  },

  [ActionTypes.LOAD_FAVORITES](context: AugmentedActionContext) {
    return new Promise<void>((resolve, reject) => {
      loadFavoritesFromIndexedDB()
        .then(data => {
          context.commit(MutationTypes.SET_FAVORITES, data);
          resolve();
        })
        .catch(reject);
    });
  },

  [ActionTypes.SAVE_FAVORITES](context: AugmentedActionContext) {
    return new Promise<void>(resolve => {
      saveToIndexedDB(context.state.settings.Favorites);
      resolve();
    });
  }
};
