import { State } from "vue";
import { CommitOptions, createStore, DispatchOptions, Store } from "vuex";
import { actions, Actions } from "./actions";
import { mutations, Mutations } from "./mutations";
import { createState } from "./state";

export const store = createStore({
  state: createState,
  mutations: mutations,
  actions: actions,
  modules: {}
});

export type CustomStore = Omit<
  Store<State>,
  "getters" | "commit" | "dispatch"
> & {
  commit<K extends keyof Mutations, P extends Parameters<Mutations[K]>[1]>(
    key: K,
    payload: P,
    options?: CommitOptions
  ): ReturnType<Mutations[K]>;
} & {
  dispatch<K extends keyof Actions>(
    key: K,
    payload: Parameters<Actions[K]>[1],
    options?: DispatchOptions
  ): ReturnType<Actions[K]>;
};
