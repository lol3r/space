import { State } from "vue";
import { MutationTree } from "vuex";
import { MutationTypes } from "@/store/mutation-types";
import {
  addToFavorites,
  removeFromFavorites
} from "@/utils/settings/favorites";
import { deleteFromIndexedDB } from "@/utils/settings/indexeddb";

export type Mutations<S = State> = {
  [MutationTypes.UPDATE_CURRENT_IMAGE](
    state: S,
    nImage: NASAImage | undefined
  ): void;
  [MutationTypes.ADD_FAVORITE](state: S, nFavorite: NASAImage): void;
  [MutationTypes.REMOVE_FAVORITE](state: S, pageURL: string): void;
  [MutationTypes.SET_FAVORITES](state: S, nFavorites: Array<NASAImage>): void;
};

export const mutations: MutationTree<State> & Mutations = {
  [MutationTypes.UPDATE_CURRENT_IMAGE](state, nImage: NASAImage | undefined) {
    state.currentImage = nImage;
  },
  [MutationTypes.ADD_FAVORITE](state, nFavorite: NASAImage) {
    const nSettings = addToFavorites(state.settings, nFavorite);
    state.settings = nSettings;
  },
  [MutationTypes.REMOVE_FAVORITE](state, pageURL: string) {
    const nSettings = removeFromFavorites(state.settings, pageURL);
    state.settings = nSettings;

    deleteFromIndexedDB(pageURL);
  },
  [MutationTypes.SET_FAVORITES](state, nFavorites: Array<NASAImage>) {
    state.settings.Favorites = nFavorites;
  }
};
