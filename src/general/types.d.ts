interface NasaDate {
  year: number;
  month: number;
  day: number;
}

interface NASAImage {
  title: string;
  pageURL: string;
  imageURL: string;
  date: NasaDate;
}

interface Settings {
  Favorites: Array<NASAImage>;
}
