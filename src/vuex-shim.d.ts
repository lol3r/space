import { CustomStore } from "./store";

declare module "@vue/runtime-core" {
  // Declare your own store states.
  interface State {
    currentImage: NASAImage | undefined;
    settings: Settings;
  }

  interface ComponentCustomProperties {
    $store: CustomStore;
  }
}
